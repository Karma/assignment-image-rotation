#ifndef AIR_FORMAT_BMP
#define AIR_FORMAT_BMP

#include <stdio.h>

#include "image.h"

typedef enum read_status {
  READ_OK = 0,
  READ_CANT_OPEN_FILE,
  READ_FILE_FAILED,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
} read_status_e;

typedef enum write_status {
  WRITE_OK = 0,
  WRITE_CANT_OPEN_FILE,
  WRITE_FILE_FAILED,
} write_status_e;

read_status_e bmp_from(FILE *in, image_s *img);
read_status_e bmp_read_from(char const *path, image_s *img);

write_status_e bmp_to(FILE *out, image_s const *img);
write_status_e bmp_write_to(char const *path, image_s const *img);

const char *status_read_to_string(read_status_e status);
const char *status_write_to_string(write_status_e status);

#endif
