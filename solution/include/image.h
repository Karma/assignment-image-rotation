#ifndef AIR_IMAGE
#define AIR_IMAGE

#include <stdint.h>

typedef struct pixel {
  uint8_t b, g, r;
} pixel_s;

typedef struct image {
  uint64_t width, height;
  pixel_s *data;
} image_s;

image_s image_create(uint64_t width, uint64_t height);
pixel_s image_get(image_s img, uint64_t x, uint64_t y);
void image_set(image_s img, uint64_t x, uint64_t y, pixel_s pixel);
void image_destroy(image_s *image);

#endif
