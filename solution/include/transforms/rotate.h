#ifndef AIR_TRANSFORM_ROTATE
#define AIR_TRANSFORM_ROTATE

#include "image.h"

// Returns a new image that is a copy of the original rotated counter-clockwise
image_s rotate(image_s const source);

#endif
