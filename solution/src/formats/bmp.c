#include "formats/bmp.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
typedef struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
} bmp_header_s;
#pragma pack(pop)

read_status_e bmp_read_from(char const *path, image_s *img) {
  FILE *file = fopen(path, "rb");
  if (!file) {
    return READ_CANT_OPEN_FILE;
  }

  read_status_e status = bmp_from(file, img);
  fclose(file);

  return status;
}

write_status_e bmp_write_to(char const *path, image_s const *img) {
  FILE *file = fopen(path, "wb");
  if (!file) {
    return WRITE_CANT_OPEN_FILE;
  }

  write_status_e status = bmp_to(file, img);
  fclose(file);

  return status;
}

static uint32_t calculate_padding(uint32_t image_width) {
  return (4 - (image_width * sizeof(pixel_s)) % 4) % 4;
}

read_status_e validate_header(bmp_header_s header);
bmp_header_s generate_header(image_s const img);
read_status_e img_read_from_bmp(FILE *in, image_s const img);
write_status_e img_write_as_bmp(FILE *out,image_s const img);

write_status_e bmp_to(FILE *out, image_s const *img) {
  const bmp_header_s header = generate_header(*img);

  // write header
  if (1 != fwrite(&header, sizeof(header), 1, out)) {
    return WRITE_FILE_FAILED;
  }

  return img_write_as_bmp(out, *img);
}

read_status_e bmp_from(FILE *in, image_s *img) {

  bmp_header_s header;
  if (1 != fread(&header, sizeof(header), 1, in)) {
    return READ_FILE_FAILED;
  }

  read_status_e status = validate_header(header);
  if (status != READ_OK) {
    return status;
  }

  image_s image = image_create(header.biWidth, header.biHeight);

  status = img_read_from_bmp(in, image);
  if (status != READ_OK) {
    image_destroy(&image);
    return status;
  }

  *img = image;
  return READ_OK;
}

static const uint16_t BMP_SIGNATURE = 0x4D42;
static const uint32_t BMP_RESERVED = 0;
static const uint32_t BMP_OFF_BITS = sizeof(bmp_header_s);
static const uint32_t BMP_ADD_HEADER_SIZE = 40;
static const uint16_t BMP_PLANES = 1;
static const uint16_t BMP_BIT_COUNT = 8 * sizeof(pixel_s);
static const uint32_t BMP_COMPRESSION = 0;
static const uint32_t BMP_PIXELS_PER_METER = 0;
static const uint32_t BMP_COLORS_USED = 0;
static const uint32_t BMP_COLORS_IMPORTANT = 0;

bmp_header_s generate_header(image_s const img) {
  const uint32_t padding = calculate_padding(img.width);
  const uint32_t image_size =
      img.height * (img.width * sizeof(pixel_s) + padding);
  return (bmp_header_s){
      BMP_SIGNATURE,             // bfType
      BMP_OFF_BITS + image_size, // bfileSize
      BMP_RESERVED,              // bfReserved
      BMP_OFF_BITS,              // bOffBits
      BMP_ADD_HEADER_SIZE,       // biSize
      img.width,                 // biWidth
      img.height,                // biHeight
      BMP_PLANES,                // biPlanes
      BMP_BIT_COUNT,             // biBitCount
      BMP_COMPRESSION,           // biCompression
      image_size,                // biSizeImage
      BMP_PIXELS_PER_METER,      // biXPelsPerMeter
      BMP_PIXELS_PER_METER,      // biYPelsPerMeter
      BMP_COLORS_USED,           // biClrUsed
      BMP_COLORS_IMPORTANT,      // biClrImportant
  };
}

read_status_e validate_header(bmp_header_s header) {
  if (header.bfType != BMP_SIGNATURE) {
    return READ_INVALID_SIGNATURE;
  }
  if (header.bOffBits != BMP_OFF_BITS) {
    return READ_INVALID_HEADER;
  }
  if (header.biBitCount != BMP_BIT_COUNT) {
    return READ_INVALID_BITS;
  }
  return READ_OK;
}

read_status_e img_read_from_bmp(FILE *in, image_s const img) {
  const uint32_t padding = calculate_padding(img.width);
  for (int y = 0; y < img.height; y++) {
    // read pixel line
    if (img.width !=
        fread(img.data + img.width * y, sizeof(pixel_s), img.width, in)) {
      free(img.data);
      return READ_FILE_FAILED;
    }

    // skip padding bytes
    if (0 != fseek(in, padding, SEEK_CUR)) {
      free(img.data);
      return READ_FILE_FAILED;
    }
  }
  return READ_OK;
}

write_status_e img_write_as_bmp(FILE *out, image_s const img) {
  const uint32_t padding = calculate_padding(img.width);
  const char padding_bytes[3] = {0};
  for (int y = 0; y < img.height; y++) {
    // write pixel line
    if (img.width !=
        fwrite(img.data + img.width * y, sizeof(pixel_s), img.width, out)) {
      return WRITE_FILE_FAILED;
    }

    // write padding bytes
    if (1 != fwrite(padding_bytes, padding, 1, out)) {
      return WRITE_FILE_FAILED;
    }
  }
  return WRITE_OK;
}

const char *status_read_message[] = {
    "READ_OK",                        //READ_OK 
    "READ_CANT_OPEN_FILE",            //READ_CANT_OPEN_FILE 
    "READ_FILE_FAILED",               //READ_FILE_FAILED 
    "READ_INVALID_SIGNATURE",         //READ_INVALID_SIGNATURE 
    "READ_INVALID_BITS",              //READ_INVALID_BITS 
    "READ_INVALID_HEADER",            //READ_INVALID_HEADER 
};
const char *status_read_to_string(read_status_e status) {
  return status_read_message[status];
}

const char *status_write_message[] = {
    "WRITE_OK",                       //WRITE_OK 
    "WRITE_CANT_OPEN_FILE",           //WRITE_CANT_OPEN_FILE 
    "WRITE_FILE_FAILED",              //WRITE_FILE_FAILED 
};
const char *status_write_to_string(write_status_e status) {
  return status_write_message[status];
}
