#include "image.h"

#include <stdlib.h>

image_s image_create(uint64_t width, uint64_t height) {
  pixel_s *data = calloc(sizeof(pixel_s), width * height);
  return (image_s){width, height, data};
}

pixel_s image_get(image_s img, uint64_t x, uint64_t y) {
  return img.data[y * img.width + x];
}

void image_set(image_s img, uint64_t x, uint64_t y, pixel_s pixel) {
  img.data[y * img.width + x] = pixel;
}

void image_destroy(image_s *image) {
  free(image->data);
  image->data = NULL;
}
