#include "transforms/rotate.h"

#include <stdlib.h>

image_s rotate(image_s const source) {
  image_s result = image_create(source.height, source.width);

  for (uint32_t x = 0; x < result.width; x++) {
    uint32_t y0 = result.width - x - 1;
    for (uint32_t y = 0; y < result.height; y++) {
      image_set(result, x, y, image_get(source, y, y0));
    }
  }

  return result;
}
