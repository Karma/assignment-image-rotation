#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"

#include "formats/bmp.h"
#include "transforms/rotate.h"

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Expected 2 arguments, but got %d!\n", argc - 1);
    return EXIT_FAILURE;
  }

  image_s source_image;

  read_status_e read_status = bmp_read_from(argv[1], &source_image);
  if (read_status != READ_OK) {
    fprintf(stderr, "Error while reading BMP source_image:\n\t%s\n",
            status_read_to_string(read_status));
    return EXIT_FAILURE;
  }

  image_s result_image = rotate(source_image);
  image_destroy(&source_image);

  write_status_e write_status = bmp_write_to(argv[2], &result_image);
  if (write_status != WRITE_OK) {
    fprintf(stderr, "Error while writing BMP result_image:\n\t%s\n",
            status_write_to_string(write_status));
    image_destroy(&result_image);
    return EXIT_FAILURE;
  }

  image_destroy(&result_image);

  return EXIT_SUCCESS;
}
